#!/usr/bin/python3

import sys
import os
import requests
import urllib.request
import ast
import pprint as pp

possible_arg_count = [2]
if (len(sys.argv) not in possible_arg_count):
    sys.exit("""
    ###Mismatch argument count. Expected 1.###

This script print rate of selected currency rate
USAGE: 
    python3 main.py gold

    python3 main.py list_currencies
    
    python3 main.py loop

    python3 main.py <currency abbreviation> 

Banking service providing data NBP
""")


def checkArg():
   possible_args = ["gold", "list_currencies", "loop"]

   if ((str(sys.argv[1]) not in possible_args) and (len(sys.argv[1]) != 3)):
      sys.exit("""
    ###Wrong usage.###

This script print rate of selected currency rate
USAGE: 
    python3 main.py gold

    python3 main.py list_currencies
    
    python3 main.py loop

    python3 main.py <currency abbreviation> 

NOTE: currency abbreviation must consist of 3 letters
Banking service providing data NBP
""")


checkArg()

if(sys.argv[1] == "gold"):
    request = "http://api.nbp.pl/api/cenyzlota/"
    with urllib.request.urlopen(request) as response:
        resp = response.read()
        dict_resp = resp.decode("UTF-8")
        data = ast.literal_eval(dict_resp)
        print("Gold price is: ", data[0].get("cena"), " PLN")
        # print(resp.decode().find("cena"))
        sys.exit("exit")

if(sys.argv[1] == "list_currencies"):
    request = "http://api.nbp.pl/api/exchangerates/tables/a/"
    with urllib.request.urlopen(request) as response:
        resp = response.read()
        dict_resp = resp.decode("UTF-8")
        data = ast.literal_eval(dict_resp)
        rate_table = data[0].get("rates")
        for rate in rate_table:
            print(' \n '.join("{}: {}".format(k, v)
                              for k, v in rate.items()))
        sys.exit("exit")

url_template = "http://api.nbp.pl/api/exchangerates/rates/a/{code}/"

if(sys.argv[1] == "loop"):
    while True:
        currency_code = input(
            "Provide currency abbreviation. Example USD \n").lower()
        request = url_template.format(code=currency_code)
        rate = requests.get(request).json()["rates"][0]["mid"]
        print("1 ", currency_code.upper(), " is equal to: ", rate, " PLN")


try:
    CURRENCY = str(sys.argv[1]).lower()
    request = url_template.format(code=CURRENCY)
    rate = requests.get(request).json()["rates"][0]["mid"]
    print("1 ", CURRENCY.upper(), " is equal to: ", rate, " PLN")
except:
    print("ERROR geting datta check if this currency exist using '$ python3 main.py list_currencies' ")

sys.exit("Done!")
